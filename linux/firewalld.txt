firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --permanent --zone=public --add-port=22/tcp

firewall-cmd --permanent --zone=public --add-port=1521/tcp


firewall-cmd --permanent --zone=public --list-services
firewall-cmd --permanent --zone=public --list-ports
