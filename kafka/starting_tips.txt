
d:

cd d:\Kafka


------------

-- start zookeeper and kafka server

start d:\Kafka\bin\windows\zookeeper-server-start.bat d:\Kafka\config\zookeeper.properties

start d:\Kafka\bin\windows\kafka-server-start.bat d:\Kafka\config\server.properties

------------

-- create a topic
d:\Kafka\bin\windows\kafka-topics.bat --create --topic test-request-ing --partitions 2 --if-not-exists --bootstrap-server localhost:9092
d:\Kafka\bin\windows\kafka-topics.bat --create --topic test-response-ing --partitions 2 --if-not-exists --bootstrap-server localhost:9092

------------

-- alter a topic
d:\Kafka\bin\windows\kafka-topics.bat --alter --topic test-kafka --partitions 2 --bootstrap-server localhost:9092

------------
-- alter topic retention time
d:\kafka\bin\windows\kafka-configs.bat --bootstrap-server localhost:9092 --alter --topic test-kafka --add-config retention.ms=86400000

------------

-- start producer

d:\Kafka\bin\windows\kafka-console-producer.bat --topic test-kafka --bootstrap-server localhost:9092

d:\Kafka\bin\windows\kafka-console-producer.bat --topic test-kafka --bootstrap-server localhost:9092 --property "parse.key=true" --property "key.separator=:"

------------

-- start consumers

d:\Kafka\bin\windows\kafka-console-consumer.bat --topic test-kafka --bootstrap-server localhost:9092 --property print.timestamp=true print.offset=true

d:\Kafka\bin\windows\kafka-console-consumer.bat --topic test-kafka --from-beginning --bootstrap-server localhost:9092 --property print.timestamp=true


------------

-- describe a topic
d:\Kafka\bin\windows\kafka-topics.bat --describe --topic test-kafka --bootstrap-server localhost:9092

------------


-- reset offset -- what is application id ????

d:\Kafka\bin\windows\kafka-streams-application-reset.bat --bootstrap-servers localhost:9092 --input-topics test-kafka --to-datetime 2021-03-04T00:00:00.000 --dry-run

----------------------

reset offset

./kafka-consumer-groups --bootstrap-server geo-ubuntu-server:9092,geo-ubuntu-server-2:9092,geo-ubuntu-server-3:9092 --all-groups -describe

./kafka-consumer-groups --bootstrap-server geo-ubuntu-server:9092,geo-ubuntu-server-2:9092,geo-ubuntu-server-3:9092 --group csr-logger-robot --reset-offsets --to-earliest --topic csr-logger-topic-v2 --execute

-- kafka-consumer-groups.sh --bootstrap-server localhost:9092 --group myConsumerGroup --reset-offsets --to-datetime 2020-12-20T00:00:00.000 --topic my_topic --execute

----------------------

-- describe consumers

kafka-consumer-groups.bat --all-groups --bootstrap-server localhost:9092 --members --describe
