select current_timestamp;

select current_date;


select date_part('year', current_timestamp) as year,
       date_part('month', current_timestamp) as month,
       date_part('day', current_timestamp) as day,
       date_part('hour', current_timestamp) as hour,
       date_part('minute', current_timestamp) as minute;

