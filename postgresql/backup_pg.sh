#!/bin/bash

today=$(date '+%Y%m%d')
exppath=/mnt/d/backup

pg_dump -d devel -h geo-pc -U postgres -f $exppath/backup_devel_$today.sql

7za a $exppath/backup_devel_$today.7z $exppath/backup_devel_$today.sql

rm $exppath/backup_devel_$today.sql
